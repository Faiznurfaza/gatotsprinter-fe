import HeaderSection from "@/components/LandingPage/HeaderSection";
import Newsletter from "@/components/LandingPage/Newsletter";
import LeaderboardSection from "@/components/LandingPage/LeaderboardSection";
import FooterSection from "@/components/LandingPage/FooterSection";
import Navbar from "@/components/Navbar/Navbar";

export default function Home() {
  return (
    <div className="main">
      <Navbar />
      <HeaderSection />
      <LeaderboardSection />
      <Newsletter />
      <FooterSection />
    </div>
  );
}
