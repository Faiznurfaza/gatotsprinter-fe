import Navbar from "@/components/Navbar/Navbar";
import Register from "@/components/Register/Register";

export default function RegisterPage() {
  return (
    <>
      <Navbar />
      <Register />
    </>
  );
}
