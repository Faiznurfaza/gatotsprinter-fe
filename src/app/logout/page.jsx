/* eslint-disable react-hooks/rules-of-hooks */
import Logout from "@/components/Logout/Logout";
import Navbar from "@/components/Navbar/Navbar";

export default function LogoutPage() {
  return (
    <>
      <Navbar />
      <Logout />
    </>
  );
}
