import React from 'react';
import UploadWidget from '@/components/Profile/UploadWidget';
import Navbar from '@/components/Navbar/Navbar';
import FooterSection from '@/components/LandingPage/FooterSection';

function UploadPhotoPage() {
    return (
        <>
            <Navbar/>
            <UploadWidget/>
            <FooterSection/>
        </>
    )
}

export default UploadPhotoPage