import ProfileEditPage from "@/components/Profile/ProfileEditPage";

export default function Profile() {
  return (
    <>
      <ProfileEditPage />
    </>
  );
}
