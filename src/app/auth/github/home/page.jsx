/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import React, { useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/navigation";
import { LoginAction } from "@/redux/features/AuthReducer";
import { useAppSelector, useAppDispatch } from "@/redux/hooks";

const GithubCallback = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();

  useEffect(() => {
    const authenticateUser = async () => {
      try {
        const response = await axios.get(
          "https://gatotsprinter-be.vercel.app/api/v1/user/auth/github"
        );
        console.log(response.data.authUrl);
        router.replace(`${response.data.authUrl}`);
      } catch (error) {
        console.error(error);
        // Handle the error, e.g., redirect to an error page or display an error message
      }
    };

    authenticateUser();
  }, []);

  return <div>Logging in...</div>;
};

export default GithubCallback;
