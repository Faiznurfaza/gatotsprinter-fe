import "./globals.css";
import { Inter } from "next/font/google";
import { Providers } from "../redux/providers";

export const metadata = {
  title: "Gatot Sprinter",
  description: "To fulfill our task in challenge 11",
};

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({ children }) {
  return (
    <html lang="en" className="h-full bg-white">
      <body className="h-full">
        <Providers>{children}</Providers>
      </body>
    </html>
  );
}
