import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  data: [],
  loading: false,
};

export const fetchGamesById = createAsyncThunk(
  "getGamesById",
  async (gameId) => {
    const response = await axios.get(
      `https://gatotsprinter-be.vercel.app/api/v1/game/id/${gameId}`
    );
    return response.data.game;
  }
);

const gameByIdSlice = createSlice({
  name: "gameById",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGamesById.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchGamesById.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(fetchGamesById.rejected, (state) => {
        state.loading = false;
        state.data = [];
      });
  },
});

export default gameByIdSlice.reducer;
