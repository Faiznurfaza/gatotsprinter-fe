import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  data: [],
  loading: false,
};

export const fetchGames = createAsyncThunk("getAllGames", async () => {
  const response = await axios.get("https://gatotsprinter-be.vercel.app/api/v1/game/all");
  return response.data.games;
});

const gamesSlice = createSlice({
  name: "games",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGames.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchGames.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(fetchGames.rejected, (state) => {
        state.loading = false;
        state.data = [];
      });
  },
});

export default gamesSlice.reducer;
