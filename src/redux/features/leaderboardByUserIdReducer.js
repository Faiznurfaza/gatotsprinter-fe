import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  data: [],
  loading: false,
};

export const fetchLeaderboardByUserId = createAsyncThunk(
  "getLeaderboardByUserId",
  async (userId) => {
    const response = await axios.get(
      `https://gatotsprinter-be.vercel.app/api/v1/score/get/${userId}`
    );
    return response.data.score;
  }
);

const leaderboarByUserIdSlice = createSlice({
  name: "leaderboardByUserId",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchLeaderboardByUserId.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchLeaderboardByUserId.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(fetchLeaderboardByUserId.rejected, (state) => {
        state.data = [];
        state.loading = false;
      });
  },
});

export default leaderboarByUserIdSlice.reducer;
