import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  data: [],
  loading: false,
};

export const fetchLeaderboardByGameId = createAsyncThunk(
  "getLeaderboardByGameId",
  async (gameId) => {
    const response = await axios.get(
      `https://gatotsprinter-be.vercel.app/api/v1/score/leaderboard/${gameId}`
    );
    return response.data.data;
  }
);

const leaderboarByGameIdSlice = createSlice({
  name: "leaderboardByGameId",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchLeaderboardByGameId.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchLeaderboardByGameId.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(fetchLeaderboardByGameId.rejected, (state) => {
        state.data = [];
        state.loading = false;
      });
  },
});

export default leaderboarByGameIdSlice.reducer;

