import { configureStore } from "@reduxjs/toolkit";
import gameListReducer from "./features/gameListReducer";
import gameByIdReducer from "./features/gameByIdReducer";
import leaderboardListReducer from "./features/leaderboardListReducer";
import leaderboardByUserIdReducer from "./features/leaderboardByUserIdReducer";
import RPSgameReducer from "./features/games/RPSgameReducer";
import GuessNumberReducer from "./features/games/GuessNumberReducer";
import AuthReducer from "./features/AuthReducer";

export const store = configureStore({
  reducer: {
    games: gameListReducer,
    leaderboard: leaderboardListReducer,
    gamesById: gameByIdReducer,
    leaderboardByUserId: leaderboardByUserIdReducer,
    rpsgame: RPSgameReducer,
    guessnumber: GuessNumberReducer,
    authreducer: AuthReducer,
  },
});
