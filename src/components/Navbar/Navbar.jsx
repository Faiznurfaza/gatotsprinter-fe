/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import Link from "next/link";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { LoginAction } from "@/redux/features/AuthReducer";
import Cookies from "js-cookie";

const Navbar = () => {
  const dispatch = useAppDispatch();
  const isLoggedIn = useAppSelector((state) => state.authreducer.isLoggedIn);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  let userId = Cookies.get("userId");

  const AuthCheck = () => {
    if (userId !== undefined) {
      dispatch(LoginAction());
    }
  };

  useEffect(() => {
    AuthCheck();
  }, [userId]);

  const toggleMobileMenu = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
  };

  return (
    <div className="bg-black text-white">
      <nav className="navbar container mx-auto px-4 py-2 flex justify-between items-center">
        <div className="nav-logo">
          <Link href="/" legacyBehavior>
            <Image
              src="/assets/icon-web.webp"
              alt="Logo"
              width={48}
              height={48}
            />
          </Link>
        </div>
        <div className="hidden md:flex md:items-center md:justify-center md:flex-grow">
          <ul className="flex space-x-4">
            <li>
              <Link
                href="/home"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                href="/games"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Games
              </Link>
            </li>
            <li>
              <Link
                href="/leaderboard"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Leaderboard
              </Link>
            </li>
          </ul>
        </div>
        <div>
          <ul className="flex space-x-4">
            {isLoggedIn ? (
              <>
                <li>
                  <Link
                    href="/profile"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Profile
                  </Link>
                </li>
                <li>
                  <Link
                    href="/logout"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Logout
                  </Link>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link
                    href="/register"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Sign Up
                  </Link>
                </li>
                <li>
                  <Link
                    href="/login"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Sign In
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
        <div className="md:hidden">
          <button
            className="text-yellow-500 hover:text-gray-300"
            onClick={toggleMobileMenu}
          >
            <svg
              className="w-6 h-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              {isMobileMenuOpen ? (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              ) : (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h16"
                />
              )}
            </svg>
          </button>
        </div>
      </nav>
      {isMobileMenuOpen && (
        <div className="md:hidden">
          <ul className="flex flex-col space-y-4 py-4">
            <li>
              <Link
                href="/home"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                href="/games"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Games
              </Link>
            </li>
            <li>
              <Link
                href="/leaderboard"
                className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
              >
                Leaderboard
              </Link>
            </li>
            {isLoggedIn ? (
              <>
                <li>
                  <Link
                    href="/profile"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Profile
                  </Link>
                </li>
                <li>
                  <Link
                    href="/logout"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Logout
                  </Link>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link
                    href="/register"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Sign Up
                  </Link>
                </li>
                <li>
                  <Link
                    href="/login"
                    className="text-yellow-500 rounded-md px-3 py-2 text-sm font-medium hover:text-gray-300"
                  >
                    Sign In
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
      )}
    </div>
  );
};

export default Navbar;
