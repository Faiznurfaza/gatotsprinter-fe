"use client";
import React, { useState, useEffect, useCallback } from "react";
import { useParams } from "next/navigation";
import ProfileShow from "./ProfileShow";
import Navbar from "../Navbar/Navbar";
import axios from "axios";

function ProfileShowPage() {
  const { id } = useParams();

  const [user, setUser] = useState({});
  const [scores, setScores] = useState([]);
  const [load, setLoad] = useState(true);

  const loadUser = useCallback(async () => {
    try {
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/user/${id}`
      );
      setUser(response.data.data);
      setLoad(false);
    } catch (err) {
      console.log(err);
    }
  }, [id]);

  const fetchUserScores = useCallback(async () => {
    try {
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/score/all`
      );
      setScores(response.data.scores);
    } catch (error) {
      console.log(error);
      setScores([]);
    }
  }, []);

  useEffect(() => {
    loadUser();
  }, [loadUser]);

  useEffect(() => {
    if (user.id) {
      fetchUserScores();
    }
  }, [user, fetchUserScores]);

  return (
    <div className="main">
      <div className="relative" style={{ zIndex: 999 }}>
        <Navbar />
      </div>
      {!load && <ProfileShow user={user} scores={scores} />}
    </div>
  );
}

export default ProfileShowPage;
