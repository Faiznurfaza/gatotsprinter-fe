"use client";

import { useState, useEffect } from "react";
import Navbar from "../Navbar/Navbar";
import Profile from "../Profile/Profile";
import axios from "axios";
import Cookie from "js-cookie";

function ProfilePage() {
  const [user, setUser] = useState({});
  const [scores, setScores] = useState([]);
  const [load, setLoad] = useState(true);

  const loadUser = async () => {
    try {
      const userId = Cookie.get("userId");
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/user/${userId}`
      );
      setUser(response.data.data);
      setLoad(false);
    } catch (err) {
      console.log(err);
    }
  };

  const fetchUserScores = async () => {
    try {
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/score/all`
      );
      setScores(response.data.scores);
    } catch (error) {
      console.log(error);
      setScores([]);
    }
  };

  useEffect(() => {
    loadUser();
  }, []);

  useEffect(() => {
    if (user.id) {
      fetchUserScores();
    }
  }, [user]);

  return (
    <div className="main">
      <div className="relative" style={{ zIndex: 999 }}>
        <Navbar />
      </div>
      {!load && <Profile user={user} scores={scores} />}
    </div>
  );
}

export default ProfilePage;
