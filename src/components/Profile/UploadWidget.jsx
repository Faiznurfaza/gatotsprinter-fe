"use client";

import React, { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import Image from "next/image";
import axios from "axios";
import Cookies from "js-cookie";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const UploadWidget = () => {
  let userId = Cookies.get("userId");
  const router = useRouter();
  useEffect(() => {
    if (!userId) {
      router.push("/login");
    }
  }, [userId, router]);

  // Use State
  const [data, setData] = useState(null);
  const [spinnerUpload, setSpinnerUpload] = useState(false);
  const [spinnerUpdate, setSpinnerUpdate] = useState(false);
  const [updateButton, setUpdateButton] = useState("Update");
  const [uploadButton, setUploadButton] = useState("Choose Image");
  const [updateInfo, setUpdateInfo] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  const handleUpdate = async (e) => {
    e.preventDefault();
    setUpdateInfo("");
    setErrorMsg("");
    try {
      setUpdateButton("");
      setSpinnerUpdate(true);
      const response = await axios.post(
        `https://gatotsprinter-be.vercel.app/api/v1/user/updatephoto/${userId}`,
        { photo: data },
        { headers: { "Content-Type": "application/json" } }
      );
      console.log(response);

      setUpdateButton("Done");
      setUpdateInfo("Your profile photo is updated !");
      setSpinnerUpdate(false);
    } catch (error) {
      setErrorMsg(error.message);
      setSpinnerUpdate(false);
      setUpdateButton("Update photo failed..");
      setTimeout(() => {
        setUpdateButton("Update");
        setData(null);
      }, 1000);
    }
  };

  const handleCancel = (e) => {
    e.preventDefault();
    setData(null);
  };

  const handleUpload = async (event) => {
    const file = event.target.files[0];
    if (file) {
      if (!file.type.startsWith("image/")) {
        setErrorMsg("Invalid file type. Please choose an image file");
        setTimeout(() => {
          setErrorMsg("");
        }, 4000);
        return;
      }
      const formData = new FormData();
      formData.append("file", file);
      formData.append("upload_preset", process.env.CLOUDINARY_CLOUD_PRESET);
      formData.append("cloud_name", process.env.CLOUDINARY_CLOUD_NAME);
      setUploadButton("");
      setSpinnerUpload(true);

      try {
        const response = await axios.post(
          `https://api.cloudinary.com/v1_1/${process.env.CLOUDINARY_CLOUD_NAME}/auto/upload`,
          formData
        );

        let responseData = response.data.secure_url;
        console.log("Upload successful");
        setData(responseData);
        setUploadButton("Done");
        setSpinnerUpload(false);

        setTimeout(() => {
          setUploadButton("Choose Image");
        }, 1000);
      } catch (error) {
        console.log(error);
        setSpinnerUpload(false);
        if (error.response.status === 400) {
          setErrorMsg("Bad Request! Please try again later..");
          setUploadButton("Upload Failed..");
        }
        setTimeout(() => {
          setErrorMsg("");
          setUploadButton("Choose Image");
        }, 4000);
      }
    }
  };

  return (
    <div className="flex flex-1 flex-col justify-center px-6 pt-0 lg:px-8 w-auto min-h-full">
      {data ? (
        <div>
          <h2 className="text-center text-2xl font-weight-600 leading-9 tracking-tight text-green-600 mb-4">
            {updateInfo !== "" ? updateInfo : "Image successfully uploaded!"}
          </h2>
          <p className="text-center text-2xl font-weight-600 leading-9 tracking-tight text-gray-900 mb-4">
            {updateInfo !== ""
              ? ""
              : "Do you want to update your profile picture?"}
          </p>
        </div>
      ) : (
        <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
          {errorMsg !== ""
            ? errorMsg
            : "Upload an Image to update your Profile Picture"}
        </h2>
      )}
      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        {data ? (
          <div className="col-lg-6 text-center justify-center">
            <Image
              src={`${data}`}
              alt="Uploaded Image"
              width={120}
              height={120}
              quality={100}
              style={imageStyle}
            />
            {updateInfo !== "" ? (
              ""
            ) : (
              <div>
                <button
                  className="bg-gray-400 hover:bg-gray-600 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow mt-16 mr-5"
                  onClick={handleCancel}
                >
                  Cancel
                </button>
                <button
                  className="bg-green-400 hover:bg-green-600 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
                  onClick={handleUpdate}
                >
                  {updateButton}{" "}
                  {spinnerUpdate ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    ""
                  )}
                </button>
              </div>
            )}
          </div>
        ) : (
          <div></div>
        )}
        <label htmlFor="upload-input" className="mb-4">
          <span className="flex flex-row justify-center items-center bg-gray-200 hover:bg-gray-300 py-2 px-4 rounded-lg cursor-pointer mt-20">
            {uploadButton}{" "}
            {spinnerUpload ? <FontAwesomeIcon icon={faSpinner} spin /> : ""}
          </span>
          <input
            id="upload-input"
            type="file"
            accept="image/*"
            className="hidden"
            onChange={handleUpload}
          />
        </label>
        <p className="text-gray-500">
          Only Images file allowed (.jpg, .jpeg, .png, .svg)
        </p>
      </div>
    </div>
  );
};

const imageStyle = {
  borderRadius: `9999px`,
  marginLeft: "auto",
  marginRight: "auto",
};
export default UploadWidget;
