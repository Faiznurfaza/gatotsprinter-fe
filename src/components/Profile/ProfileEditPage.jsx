"use client";
import { useState, useEffect } from "react";
import axios from "axios";
import Cookie from "js-cookie";
import ProfileEdit from "../Profile/ProfileEdit";
import Navbar from "../Navbar/Navbar";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function ProfileEditPage() {
  const [user, setUser] = useState({});
  const [load, setLoad] = useState(true);
  const [editUsername, setEditUsername] = useState(false);
  const [editEmail, setEditEmail] = useState(false);
  const [editPassword, setEditPassword] = useState(false);
  const [status, setStatus] = useState(null);
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [saveButton, setSaveButton] = useState("Save");
  const [loading, setLoading] = useState(false)

  const loadUser = async () => {
    try {
      let userId = Cookie.get("userId");
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/user/${userId}`
      );
      setUser(response.data.data);
      setLoad(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleInputChange = (event) => {
    event.persist();
    setFormData((prevFormData) => ({
      ...prevFormData,
      [event.target.name]: event.target.value,
    }));
    console.log(event.target.value);
  };

  const handleEditUsernameToggle = () => {
    setEditUsername(!editUsername);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleEditEmailToggle = () => {
    setEditEmail(!editEmail);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleEditPasswordToggle = () => {
    setEditPassword(!editPassword);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleUpdateUsername = async (event) => {
    event.preventDefault();
    try {
      setSaveButton("");
      setLoading(true)
      let userId = Cookie.get("userId");

      const updatedUser = {
        username: formData.username,
      };

      if (!updatedUser.username) {
        toast.warn("Username cannot be empty.");
        return;
      }

      const response = await axios.put(
        `https://gatotsprinter-be.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );

      if (user && updatedUser.username === user.username) {
        toast.info("Username is the same as before.");
        return;
      }

      setStatus(response.status);

      const username = updatedUser.username; // Extract the username
      console.log(username);
      setUser({
        username,
        email: user.email,
      }); // Update user information with the extracted username

      setLoading(false)
      setEditUsername(false);
      toast.success("Username updated successfully!");
    } catch (err) {
      console.log(err);
      setLoading(false)
      toast.error("Failed to update username.");
    }
  };

  const handleUpdateEmail = async (event) => {
    event.preventDefault();
    try {
      setLoading(true)
      let userId = Cookie.get("userId");

      const updatedUser = {
        email: formData.email,
      };

      if (!updatedUser.email) {
        toast.warn("Email cannot be empty.");
        return;
      }

      const response = await axios.put(
        `https://gatotsprinter-be.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );

      if (user && updatedUser.email === user.email) {
        toast.info("Email is the same as before.");
        return;
      }

      setStatus(response.status);

      const email = updatedUser.email; // Extract the email
      console.log(response);

      setLoading(false)
      setUser({
        username: user.username,
        email,
      }); // Update user information with the extracted email

      setEditEmail(false);
      toast.success("Email updated successfully!");
    } catch (err) {
      console.log(err);
      setLoading(false)
      toast.error("Failed to update email.");
    }
  };

  const handleUpdatePassword = async (event) => {
    event.preventDefault();
    try {
      setLoading(true)
      let userId = Cookie.get("userId");
      const updatedUser = {
        oldPassword: formData.oldPassword,
        newPassword: formData.newPassword,
      };

      const response = await axios.put(
        `https://gatotsprinter-be.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );
      setStatus(response.status);
      console.log(response);
      setUser({
        username: user.username,
        email: user.email,
      });
      setLoading(false)
      setSaveButton("Saved");

      setTimeout(() => {
        setSaveButton("Save");
      }, 1000);
      setEditPassword(false);
      toast.success("Password updated successfully!");
    } catch (err) {
      console.log(err);
      toast.error("Failed to update password.");
      setLoading(false)
      setSaveButton("Save");

      setTimeout(() => {
        setSaveButton("Save");
      }, 1000);
    }
  };

  useEffect(() => {
    loadUser();
  }, []);

  return (
    <div className="main">
      <div className="relative" style={{ zIndex: 999 }}>
        <Navbar />
      </div>
      {!load && (
        <ProfileEdit
          user={user}
          editUsername={editUsername}
          editEmail={editEmail}
          editPassword={editPassword}
          formData={formData}
          handleInputChange={handleInputChange}
          handleEditUsernameToggle={handleEditUsernameToggle}
          handleEditEmailToggle={handleEditEmailToggle}
          handleEditPasswordToggle={handleEditPasswordToggle}
          handleUpdateUsername={handleUpdateUsername}
          handleUpdateEmail={handleUpdateEmail}
          handleUpdatePassword={handleUpdatePassword}
          status={status}
          saveButton={saveButton}
          loading={loading}
        />
      )}
    </div>
  );
}

export default ProfileEditPage;
