"use client";
import React from "react";
import Link from "next/link";
import Image from "next/image";

function ProfileShow({ user, scores }) {
  const initials = user.username ? user.username.slice(0, 2).toUpperCase() : "";

  const rockPaperScissorsScore = scores.find(
    (score) => score.gameId === 1 && score.userId === user.id
  );
  const guessTheNumberScore = scores.find(
    (score) => score.gameId === 2 && score.userId === user.id
  );
  const formattedDate = new Date(user.createdAt).toLocaleDateString("en-US", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });

  const getRank = (score) => {
    if (score < 50) {
      return "Newcomers";
    } else if (score < 100) {
      return "Bronze Rank";
    } else if (score < 200) {
      return "Silver Rank";
    } else if (score < 300) {
      return "Gold Rank";
    } else if (score < 500) {
      return "Platinum Rank";
    } else if (score < 1000) {
      return "Diamond Rank";
    } else if (score < 1500) {
      return "Ruby Rank";
    } else {
      return "Legend Rank";
    }
  };

  const highestScore = Math.max(
    rockPaperScissorsScore?.score || 0,
    guessTheNumberScore?.score || 0
  );
  const rank = getRank(highestScore);
  const rankRPS = getRank(rockPaperScissorsScore?.score || 0);
  const rankGuess = getRank(guessTheNumberScore?.score || 0);

  return (
    <div
      className="flex flex-col items-center h-screen bg-cover bg-center absolute inset-0"
      style={{
        backgroundImage: `url('/assets/main-bg.jpg')`,
        zIndex: 0,
      }}
    >
      <div className="w-full max-w-md p-8 bg-yellow-700 bg-opacity-80 mb-20 mt-24 sm:rounded-3xl relative">
        <div
          className="absolute inset-0 bg-gradient-to-r from-yellow-300 to-yellow-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"
          style={{ zIndex: -1 }}
        ></div>

        <div className="flex justify-center">
          {!user.photo ? (
            <div className="w-32 h-32 rounded-full mx-auto flex items-center justify-center bg-gray-500 text-white text-6xl font-bold">
              {initials}
            </div>
          ) : (
            <Image
              src={user.photo}
              alt="Profile picture"
              width={80}
              height={80}
              style={imageStyle}
              quality={100}
            />
          )}
        </div>
        <h2 className="text-center text-2xl text-white font-semibold mt-3">
          {user.username?.toUpperCase()}
        </h2>
        <p className="text-center text-white mt-1">{rank}</p>

        <div className="mt-5">
          <h3 className="text-xl text-white font-semibold">Player Info</h3>
          <table className="text-white mt-2 mx-4 ">
            <tbody>
              <tr>
                <td>Registered:</td>
                <td className="px-4">{formattedDate}</td>
              </tr>
              <tr>
                <td>Score Rock Paper Scissor:</td>
                <td className="px-4">
                  {rockPaperScissorsScore?.score || 0} ({rankRPS})
                </td>
              </tr>
              <tr>
                <td>Score Guess The Number:</td>
                <td className="px-4">
                  {guessTheNumberScore?.score || 0} ({rankGuess})
                </td>
              </tr>
            </tbody>
          </table>
          <div className="mt-6 -mx-4 flex justify-end">
            <Link href="./home">
              <button className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-blue-500 hover:bg-blue-700 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer">
                Back Home
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

const imageStyle = {
  borderRadius: `9999px`,
  marginLeft: "auto",
  marginRight: "auto",
  objectFit: "cover",
};

export default ProfileShow;
