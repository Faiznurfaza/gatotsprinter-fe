"use client";

import React, { useState } from "react";
import axios from "axios";
import ForgotPasswordForm from "./ForgotPasswordForm";
import Link from "next/link";

function ForgotPasswordPage() {
  const [email, setEmail] = useState("");

  const [submit, setSubmit] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const [resetButton, setResetButton] = useState("Reset");
  const [loading, setLoading] = useState(false);

  const onInputChange = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrorMsg("");
    try {
      setResetButton("");
      setLoading(true);

      const res = await axios.post(
        "https://gatotsprinter-be.vercel.app/api/v1/user/forgot-password",
        { email: email }
      );

      setLoading(false);
      setResetButton("Done");
      setSubmit(true);
    } catch (err) {
      console.log(err);
      setLoading(false);
      if (err.response.status === 404) {
        setResetButton("Reset Failed");
        setErrorMsg(
          "Email address not registered, try again with registered email address"
        );
      } else if (err.response.status === 500) {
        setResetButton("Reset Failed");
        setErrorMsg("Unexpected error happens, please try again later.");
      } else {
        setResetButton("Reset Failed");
        setErrorMsg("Internal server error");
      }

      setTimeout(() => {
        setResetButton("Reset");
      }, 1000);
    }
  };

  function ShowResults() {
    return (
      <div style={styles.resultContainer}>
        <h4>Reset Password Code is sent to your registered email</h4>
        <Link href="/resetpassword">Reset Password Here</Link>
      </div>
    );
  }
  return (
    <div className="main">
      <ForgotPasswordForm
        email={email}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        ShowResults={ShowResults}
        submit={submit}
        resetButton={resetButton}
        loading={loading}
        errorMsg={errorMsg}
      />
    </div>
  );
}

const styles = {
  resultContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default ForgotPasswordPage;
