"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import LoginForm from "./LoginForm";
import axios from "axios";
import Cookies from "js-cookie";
import { LoginAction } from "@/redux/features/AuthReducer";
import { useAppDispatch } from "@/redux/hooks";

export default function Login() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const isLoggedIn = Cookies.get("userId");
  if (isLoggedIn) {
    router.push("/home");
  }

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const [signInButton, setSignInButton] = useState("Sign in");
  const [loading, setLoading] = useState(false);
  const [loginInfo, setLoginInfo] = useState("");

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setUser((prevState) => ({ ...prevState, [name]: value }));
  };

  const forgotPassword = (e) => {
    router.push("/forgotpassword");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoginInfo("");
    try {
      setSignInButton("");
      setLoading(true);

      const res = await axios.post(
        "https://gatotsprinter-be.vercel.app/api/v1/user/login",
        user
      );
      const userId = res.data.userId;
      const expirationAge = 7;
      Cookies.set("userId", userId, { expires: expirationAge });

      setSignInButton("Done");

      setLoading(false);
      router.push("/home");

      dispatch(LoginAction());
    } catch (error) {
      setLoading(false);
      console.log(error.response.data.msg);
      const err = error.response.data.msg;
      if (err === "Email Tidak Ditemukan") {
        setLoginInfo("Sign in failed : Wrong email or email not found !");
      } else if (err === "Password Salah") {
        setLoginInfo("Sign in failed : Wrong password !");
      } else {
        setLoginInfo("Sign in failed : Internal Server Error");
      }

      setSignInButton("Sign in Failed!");

      setTimeout(() => {
        setSignInButton("Sign in");
      }, 2000);
    }
  };

  return (
    <>
      <LoginForm
        user={user}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        forgotPassword={forgotPassword}
        signInButton={signInButton}
        loading={loading}
        loginInfo={loginInfo}
      />
    </>
  );
}
