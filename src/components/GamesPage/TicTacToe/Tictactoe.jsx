"use client";

import React, { useState, useEffect } from "react";
import styles from "../TicTacToe/board.css";
import axios from "axios";
import Cookies from "js-cookie";

function Square({ value, onSquareClick }) {
  return (
    <button className="square" onClick={onSquareClick}>
      {value}
    </button>
  );
}

function getRandomMove(squares) {
  const availableMoves = squares
    .map((value, index) => (value === null ? index : null))
    .filter((index) => index !== null);
  const randomIndex = Math.floor(Math.random() * availableMoves.length);
  return availableMoves[randomIndex];
}

export default function Tictactoe() {
  const userId = Cookies.get("userId");

  const [Info, setInfo] = useState("");
  const [winner, setWinner] = useState("");
  const [squares, setSquares] = useState(Array(9).fill(null));
  const [isPlayerTurn, setIsPlayerTurn] = useState(true);

  useEffect(() => {
    if (!isPlayerTurn) {
      const timer = setTimeout(makeComputerMove, 500);
      return () => clearTimeout(timer);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPlayerTurn]);

  function resetGame() {
    setInfo("");
    setSquares(Array(9).fill(null));
    setIsPlayerTurn(true);
    setWinner("");
  }
  function makeComputerMove() {
    if (Info !== "") return null;
    const nextSquares = squares.slice();
    const computerMove = getRandomMove(squares);
    nextSquares[computerMove] = "O";
    setSquares(nextSquares);
    setIsPlayerTurn(true);
    CheckWinner(nextSquares, resetGame);
  }

  function CheckWinner(squares, resetGame) {
    const winningCombinations = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    const players = ["X", "O"];

    for (let i = 0; i < winningCombinations.length; i++) {
      const [a, b, c] = winningCombinations[i];

      for (let player of players) {
        if (
          squares[a] === player &&
          squares[a] === squares[b] &&
          squares[a] === squares[c]
        ) {
          if (player === "X") {
            setInfo("You won, press the reset button to play again");
            setWinner("Player");
            sendScoreToAPI();
          } else {
            setInfo("You lose, press the reset button to play again");
            setWinner("Computer");
          }
          return;
        }
      }
    }

    if (!squares.includes(null)) {
      setInfo("Draw, press the reset button to play again");
      setWinner("Draw");
    }
  }

  function handleClick(i) {
    if (squares[i] || !isPlayerTurn || Info !== "") return null;
    const nextSquares = squares.slice();
    nextSquares[i] = "X";
    setSquares(nextSquares);
    setIsPlayerTurn(false);
    CheckWinner(nextSquares, resetGame);
  }

  const sendScoreToAPI = async () => {
    try {
      await axios.post(
        `https://gatotsprinter-be.vercel.app/api/v1/score/add/${userId}/3`
      );
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="game-board">
      <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900 mb-4">
        Tic-Tac-Toe
      </h2>
      {Info !== "" && (
        <h3
          className={`text-center text-base font-bold leading-9 tracking-tight ${
            winner === "Player" ? "text-green-600" : "text-red-600"
          }`}
        >
          {Info}
        </h3>
      )}
      <div className="board-row">
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
      <div className="board-row">
        <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
        <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
        <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
      </div>
      <div className="board-row">
        <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
        <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
        <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
      </div>
      {Info !== "" && (
        <button
          onClick={resetGame}
          className="flex justify-center rounded-md mt-8 bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Reset Game
        </button>
      )}
    </div>
  );
}
