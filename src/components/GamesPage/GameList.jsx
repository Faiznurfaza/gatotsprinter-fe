/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import Image from "next/image";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { useEffect, useState } from "react";
import { fetchGames } from "@/redux/features/gameListReducer";
import Link from "next/link";
import Cookies from "js-cookie";
import { fetchLeaderboardByUserId } from "@/redux/features/leaderboardByUserIdReducer";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GameList() {
  const dispatch = useAppDispatch();
  const userId = Cookies.get("userId");

  const games = useAppSelector((state) => state.games.data);
  const score = useAppSelector((state) => state.leaderboardByUserId.data);

  const [learnMore, setLearnMore] = useState("Learn More");
  const [playNow, setplayNow] = useState("Play Now");
  const [playAgain, setPlayAgain] = useState("Play Again");

  const [Spinner1, setSpinner1] = useState("");
  const [Spinner2, setSpinner2] = useState("");
  const [Spinner3, setSpinner3] = useState("");

  useEffect(() => {
    dispatch(fetchGames());
    dispatch(fetchLeaderboardByUserId(userId));
  }, [dispatch, userId]);

  const hasPlayedGame = (gameId) => {
    return score.some((score) => score.gameId === gameId);
  };

  const handleClickLearnMore = () => {
    setLearnMore("");
    setSpinner1(faSpinner);
  };
  const handleClickPlayAgain = () => {
    setPlayAgain("");
    setSpinner2(faSpinner);
  };
  const handleClickPlayNow = () => {
    setplayNow("");
    setSpinner3(faSpinner);
  };

  return (
    <div className="mb-14">
      <div className="md:flex flex-wrap max-w-6xl mx-auto justify-center text-white">
        {games.loading ? (
          <div className="text-white text-4xl w-100 my-36 mx-auto leading-normal">
            Loading...
          </div>
        ) : (
          [...games]
            .sort((a, b) => a.id - b.id)
            .map((game, index) => (
              <div
                key={index}
                className="game-card bg-[#511866] my-5 px-8 py-6 mx-auto md:mx-3 w-56 md:w-60 text-center rounded-xl"
              >
                <Image
                  className="mx-auto object-cover h-28 md:h-36"
                  src={game.thumbnail}
                  width={260}
                  height={100}
                  alt=""
                />
                <h4 className="uppercase text-sm md:text-base font-medium tracking-wide my-3">
                  {game.title}
                </h4>
                <Link
                  href={`/games/${game.id}`}
                  className="text-xs md:text-sm mt-4 font-normal opacity-50"
                  onClick={handleClickLearnMore}
                >
                  {learnMore} <FontAwesomeIcon icon={Spinner1} spin />
                </Link>
                <br />
                <br />
                {userId && hasPlayedGame(game.id) ? (
                  <Link
                    href={`/games/${game.id}/play`}
                    className="bg-[#79179c] hover:bg-[#9b27c5] text-xs md:text-sm py-2.5 px-8 rounded-full mt-8 uppercase"
                    onClick={handleClickPlayAgain}
                  >
                    {playAgain} <FontAwesomeIcon icon={Spinner2} spin />
                  </Link>
                ) : (
                  <Link
                    href={`/games/${game.id}/play`}
                    className="bg-[#79179c] hover:bg-[#9b27c5] text-xs md:text-sm py-2.5 px-8 rounded-full mt-8 uppercase"
                    onClick={handleClickPlayNow}
                  >
                    {playNow} <FontAwesomeIcon icon={Spinner3} spin />
                  </Link>
                )}
              </div>
            ))
        )}
      </div>
    </div>
  );
}
