"use client";

import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import {
  playGuess,
  updateGuess,
  updateRandomNumber,
  finishGame,
  updateTries,
  resetGame,
  updateInfo,
} from "@/redux/features/games/GuessNumberReducer";
import GuessNumber from "./GuessNumber";
import axios from "axios";
import Cookies from "js-cookie";

function GuessLogic() {
  const dispatch = useAppDispatch();
  const { maxTries, currentTries, guess, randomNumber, isGameFinished } =
    useAppSelector((state) => state.guessnumber);

  useEffect(() => {
    if (randomNumber === 0) {
      const newRandomNumber = Math.floor(Math.random() * 100) + 1;
      dispatch(updateRandomNumber(newRandomNumber));
    }
  }, [dispatch, randomNumber]);

  useEffect(() => {
    if (currentTries === 10) {
      dispatch(
        finishGame(
          "You have already tried as much as you can. Reset the game to start again"
        )
      );
    }
  }, [dispatch, currentTries]);

  const handleInputChange = (e) => {
    dispatch(updateGuess(parseInt(e.target.value, 10)));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (currentTries !== maxTries) {
      dispatch(playGuess("Game Starting"));
      if (guess === randomNumber) {
        dispatch(updateTries());
        dispatch(
          finishGame(
            `Congratulations! You guessed the correct number ${randomNumber}`
          )
        );
        if (!isGameFinished) {
          sendScoreToAPI();
        }
      } else if (guess < randomNumber) {
        dispatch(updateGuess(guess));
        dispatch(updateTries());
        dispatch(updateInfo("Try higher number"));
      } else if (guess > randomNumber) {
        dispatch(updateGuess(guess));
        dispatch(updateTries());
        dispatch(updateInfo("Try lower number"));
      }
    } else if (currentTries === maxTries) {
      dispatch(updateInfo("You have already tried as much as you can"));
    }
  };

  const sendScoreToAPI = async () => {
    try {
      const userId = Cookies.get("userId");
      await axios.post(`https://gatotsprinter-be.vercel.app/api/v1/score/add/${userId}/2`);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="main">
      <GuessNumber
        handleSubmit={handleSubmit}
        handleInputChange={handleInputChange}
        guess={guess}
      />
    </div>
  );
}

export default GuessLogic;
