"use client";

import React, { useState } from "react";
import Cookies from "js-cookie";
import axios from "axios";
import { useRouter } from "next/navigation";

function DummyGame() {
  let userId = Cookies.get("userId");
  const router = useRouter();
  const [loading, setLoading] = useState(false)

  const getScore = async () => {
    try {
      if (userId !== undefined) {
        setLoading(true)
        await axios.post(
          `https://gatotsprinter-be.vercel.app/api/v1/score/add/${userId}/3`
        );
        await axios.post(
          `https://gatotsprinter-be.vercel.app/api/v1/score/add/${userId}/4`
        );

        setLoading(false)
      } else {
        router.push("/login");
      }
    } catch (err) {
      console.log(err);
      setLoading(false)
    }
  };

  return (
    <>
      <div className="flex flex-col items-center justify-center min-h-screen bg-blue-200">
        <div className="relative">
          <button
            className="text-black flex items-center justify-center w-24 h-10 hover:text-red-300"
            onClick={() => getScore()}
            disabled={loading}
          >
            Get Score
          </button>
        </div>
      </div>
    </>
  );
}

export default DummyGame;
