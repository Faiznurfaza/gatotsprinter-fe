"use client";

import Home from "./Home";
import Cookie from "js-cookie";
import { useState, useEffect } from "react";
import axios from "axios";
import { useRouter, useSearchParams } from "next/navigation";
import { useAppDispatch } from "@/redux/hooks";
import { LoginAction } from "@/redux/features/AuthReducer";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Cookies from "js-cookie";

const HomePage = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const searchParams = useSearchParams();
  let userId = Cookie.get("userId");

  const getUser = async () => {
    try {
      setLoading(true);

      let idFromUrl = searchParams.get("userId");

      if (idFromUrl && !userId) {
        const res = await axios.get(
          `https://gatotsprinter-be.vercel.app/api/v1/user/${idFromUrl}`
        );
        const data = res.data.data;
        Cookies.set("userId", idFromUrl, { expires: 7 });
        dispatch(LoginAction());
        setUser(data);
        setLoading(false);
      } else if (!idFromUrl && userId) {
        const res = await axios.get(
          `https://gatotsprinter-be.vercel.app/api/v1/user/${userId}`
        );
        const data = res.data.data;
        dispatch(LoginAction());
        setUser(data);
        setLoading(false);
      } else {
        router.push("/login");
      }
    } catch (error) {
      if(error.response.status === 400) {
        Cookies.remove("userId")
        router.push('/login')
      } else if(error.response.status === 500) {
        Cookies.remove("userId")
        router.push('/login')
      }
      setLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return (
      <div className="flex justify-center items-center mt-48">
        <FontAwesomeIcon icon={faSpinner} spin size="2xl" />
      </div>
    );
  }
  const initials = user?.username ? user?.username.slice(0, 2).toUpperCase() : "";
  return <Home user={user} initials={initials}></Home>;
};

export default HomePage;
