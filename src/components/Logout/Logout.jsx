"use client";

import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import axios from "axios";
import LogoutPopup from "./LogoutPopup";
import { useRouter } from "next/navigation";
import { LogoutAction } from "@/redux/features/AuthReducer";
import { useAppSelector, useAppDispatch } from "@/redux/hooks";



export default function Logout() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const [logoutButton, setLogoutButton] = useState("Logout");
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    const userId = Cookies.get("userId");
    if (!userId) {
      router.push("/login");
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const handleLogout = async (e) => {
    e.preventDefault();
    try {
      setLogoutButton("");
      setLoading(true)
      const res = await axios.post(
        "https://gatotsprinter-be.vercel.app/api/user/logout"
      );
      Cookies.remove("userId");

      setLoading(false)
      setLogoutButton("Done");
      router.push("/login");
      dispatch(LogoutAction());
    } catch (err) {
      console.log(err);
      setLoading(false)

      setLogoutButton("Logout failed");

      setTimeout(() => {
        setLogoutButton("Logout");
      }, 1000);
    }
  };

  const handleCancel = (e) => {
    e.preventDefault();
    router.push("/home");
  };

  return (
    <div>
      <LogoutPopup
        handleLogout={handleLogout}
        handleCancel={handleCancel}
        logoutButton={logoutButton}
        loading={loading}
      />
    </div>
  );
}
