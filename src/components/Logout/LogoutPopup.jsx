import React from "react";
//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

export default function LogoutPopup({
  handleLogout,
  handleCancel,
  logoutButton,
  loading,
}) {
  return (
    <div style={styles.container}>
      <div className="col-lg-6 text-center">
        <h2 className="mb-5" style={{ letterSpacing: "1px" }}>
          Are you sure want to logout ?
        </h2>

        <button
          variant="primary"
          className="logout-button me-4"
          onClick={handleLogout}
        >
          {logoutButton}{" "}
          {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : ""}
        </button>
        <button
          variant="danger"
          className="cancel-button ms-4"
          onClick={handleCancel}
        >
          Cancel
        </button>
      </div>
    </div>
  );
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  box: {
    width: "80%",
    padding: "20px",
    backgroundColor: "rgba(0,0,0, 0.0)",
    marginBottom: "20px",
    marginLeft: "20rem",
    borderRadius: "10px",
  },
};
