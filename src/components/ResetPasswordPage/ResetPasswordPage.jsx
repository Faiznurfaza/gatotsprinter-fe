"use client";

import React, { useState } from "react";
import ResetPasswordForm from "./ResetPasswordForm";
import axios from "axios";

function ResetPasswordPage() {
  const [data, setData] = useState({
    resetToken: "",
    password: "",
  });

  const [submit, setSubmit] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const [resetButton, setResetButton] = useState("Reset");
  const [loading, setLoading] = useState(false);

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setData((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    setErrorMsg("");
    e.preventDefault();
    try {
      setResetButton("");
      setLoading(true);

      const res = await axios.post(
        "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password",
        data
      );

      setLoading(false);
      setResetButton("Done");

      setSubmit(true);
    } catch (err) {
      console.log(err);
      if (
        err.response.data.error === "Password must be longer than 5 characters"
      ) {
        setErrorMsg(
          "Reset Password Failed : Password must be longer than 5 characters"
        );
      } else if (err.response.data.msg === "Token reset password tidak valid") {
        setErrorMsg("Reset Password Failed, your reset token is invalid");
      } else if (err.response.status === 500) {
        setErrorMsg("Reset Password Failed, try again later");
      }
      setLoading(false);
      setResetButton("Reset Password Failed");

      setTimeout(() => {
        setResetButton("Reset");
      }, 1000);
    }
  };

  function ShowResults() {
    return (
      <div style={styles.resultContainer}>
        <h4>
          Reset Password Success, you can go try login again with your new
          password.
        </h4>
        <a href="/login">LOGIN HERE</a>
      </div>
    );
  }
  return (
    <div className="main">
      <ResetPasswordForm
        data={data}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        ShowResults={ShowResults}
        submit={submit}
        resetButton={resetButton}
        loading={loading}
        errorMsg={errorMsg}
      />
    </div>
  );
}

const styles = {
  resultContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default ResetPasswordPage;
