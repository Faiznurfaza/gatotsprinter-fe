"use client";
import React from "react";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function ResetPasswordForm({
  data,
  onSubmit,
  onInputChange,
  ShowResults,
  submit,
  resetButton,
  loading,
  errorMsg,
}) {
  return (
    <div className="flex flex-1 flex-col justify-center px-6 pt-20 lg:px-8">
      <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
        {errorMsg ? (
          <h2 className="text-md text-center font-bold mb-4 text-red-400">
            {errorMsg}
          </h2>
        ) : (
          ""
        )}
        <form className="space-y-6" onSubmit={onSubmit}>
          <h2 className="text-sm text-center mb-4 text-black">
            Enter your registered email
          </h2>
          <div>
            <label
              htmlFor="code"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Reset Code
            </label>
            <div className="mt-2">
              <input
                id="resetToken"
                name="resetToken"
                type="text"
                value={data.resetToken}
                onChange={onInputChange}
                required
                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
          <div className="mt-2">
            <label
              htmlFor="password"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              New Password
            </label>
            <input
              id="password"
              name="password"
              type="password"
              value={data.password}
              onChange={onInputChange}
              required
              className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            />
          </div>
          <button
            type="submit"
            className="reset-button w-full mt-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            {resetButton}{" "}
            {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : ""}
          </button>
        </form>
        {submit && <ShowResults />}
      </div>
    </div>
  );
}

export default ResetPasswordForm;
