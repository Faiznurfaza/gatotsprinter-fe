"use client";

import React, { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import Image from "next/image";
import axios from "axios";
import Cookies from "js-cookie";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

export default function VideoUpload() {
  let userId = Cookies.get("userId");
  const router = useRouter();
  useEffect(() => {
    if (!userId) {
      router.push("/login");
    }
  }, [userId, router]);

  // Use State
  const [data, setData] = useState(null);
  const [uploadInfo, setUploadInfo] = useState("");
  const [errorMsg, setErrorMsg] = useState("");
  useEffect(() => {
    console.log(data);
  }, [data]);
  const handleUpload = async (event) => {
    const file = event.target.files[0];
    if (!file) {
      return;
    }

    if (!file.type.startsWith("video/")) {
      setErrorMsg("Invalid file type. Please choose a video file.");
      setTimeout(() => {
        setErrorMsg("");
      }, 4000);
      return;
    }

    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", process.env.CLOUDINARY_CLOUD_PRESET);
    formData.append("cloud_name", process.env.CLOUDINARY_CLOUD_NAME);

    try {
      const response = await axios.post(
        `https://api.cloudinary.com/v1_1/${process.env.CLOUDINARY_CLOUD_NAME}/auto/upload`,
        formData
      );

      let responseVideo = response.data.secure_url;
      console.log("Upload Successful", responseVideo);
      setData(responseVideo);
      console.log(data);
      await AddVideo(responseVideo);
    } catch (error) {
      console.log(error);
    }
  };

  const AddVideo = async (video) => {
    try {
      const response = await axios.post(
        `
        https://gatotsprinter-be.vercel.app/api/v1/video/add/${userId}`,
        { video: video },
        { headers: { "Content-Type": "application/json" } }
      );
      console.log("Upload Berhasil", response);
      setUploadInfo("Upload Video Success");
    } catch (error) {
      console.log(error);
      if (error.response.status === 400) {
        setErrorMsg("Cant add any more videos");
      } else {
        setErrorMsg("Internal Server Error");
      }
      setTimeout(() => {
        setErrorMsg("");
        setData(null);
      }, 10000);
    }
  };

  return (
    <>
      <div className="text-center flex flex-1 flex-col justify-center items-center px-6 pt-0 lg:px-8 w-auto min-h-full">
        {uploadInfo !== "" ? (
          <div>
            <h2 className="text-2xl font-weight-600 leading-9 tracking-tight text-green-600 mb-8">
              {uploadInfo}
            </h2>
            <Link
              href="/video/list"
              className="font-semibold text-md text-blue-500 hover:text-blue-700"
            >
              <FontAwesomeIcon icon={faArrowRight} /> Go to your videos list
            </Link>
          </div>
        ) : (
          ""
        )}
        {errorMsg !== "" ? (
          <p className="font-bold text-lg text-red-500 mt-12">{errorMsg}</p>
        ) : (
          ""
        )}
        <label htmlFor="video-upload" className="block mb-2 font-medium">
          Upload Video:
        </label>
        <input
          id="video-upload"
          type="file"
          accept="video/*"
          onChange={handleUpload}
          className="border rounded-md py-2 px-4"
        />
      </div>
    </>
  );
}
