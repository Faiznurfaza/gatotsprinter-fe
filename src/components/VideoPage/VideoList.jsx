"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload, faTrash } from "@fortawesome/free-solid-svg-icons";
import ReactPlayer from "react-player";
import Link from "next/link";

export default function VideoList() {
  const router = useRouter();
  const [videos, setVideos] = useState([]);
  const userId = Cookies.get("userId");

  useEffect(() => {
    if (!userId) {
      router.push("/login");
    } else {
      fetchVideos();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userId, router]);

  const fetchVideos = async () => {
    try {
      const response = await axios.get(
        `https://gatotsprinter-be.vercel.app/api/v1/video/${userId}`
      );
      setVideos(response.data.data);
    } catch (error) {
      console.log(error);
      setVideos([]);
    }
  };

  const handleDelete = async (videoId) => {
    try {
      await axios.post(
        `https://gatotsprinter-be.vercel.app/api/v1/video/delete/${userId}/${videoId}`
      );

      if (videos.length === 1) {
        fetchVideos();
      } else {
        setVideos(videos.filter((video) => video.id !== videoId));
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center px-6 pt-0 lg:px-8 min-h-screen">
      <h1 className="text-2xl font-semibold mb-8">Video List</h1>
      {videos.length === 0 ? (
        <div className="text-center">
          <p className="mb-4">No videos found. Upload a video</p>
          <Link
            href="/video/upload"
            className="text-blue-500 hover:text-blue-800 font-semibold"
          >
            <FontAwesomeIcon icon={faUpload} /> Upload Video
          </Link>
        </div>
      ) : (
        <div className="grid grid-cols-3 gap-8">
          {videos.map((video, index) => (
            <div
              key={video.id}
              className="bg-white p-4 rounded-md shadow-md text-center"
            >
              <h2 className="text-lg font-bold mb-4">{index + 1}</h2>
              <div className="mb-4">
                <ReactPlayer
                  url={video.Video_URL}
                  controls
                  width="360px"
                  height="180px"
                  config={{
                    youtube: {
                      playerVars: { origin: "https://www.youtube.com" },
                    },
                  }}
                />
              </div>
              <div className="flex justify-center items-center">
                <button
                  className="text-red-500 hover:text-red-800 font-semibold"
                  onClick={() => handleDelete(video.id)}
                >
                  <FontAwesomeIcon icon={faTrash} /> Delete
                </button>
              </div>
            </div>
          ))}
          {videos.length < 3 && (
            <div className="bg-white p-4 rounded-md shadow-md text-center">
              <h2 className="text-lg font-bold mb-4">Add a Video</h2>
              <div className="mb-4">
                <p>No video found. Upload a video to get started.</p>
              </div>
              <div className="flex justify-center items-center">
                <Link
                  href="/video/upload"
                  className="text-blue-500 hover:text-blue-800 font-semibold"
                >
                  <FontAwesomeIcon icon={faUpload} /> Upload Video
                </Link>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}
