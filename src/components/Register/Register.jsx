"use client";

import React, { useState } from "react";
import { useRouter } from "next/navigation";
import RegisterForm from "../Register/RegisterForm";
import axios from "axios";
import { useAppSelector } from "@/redux/hooks";

function Register() {
  const router = useRouter();
  const loginChecker = useAppSelector((state) => state.authreducer.isLoggedIn);
  if (loginChecker) {
    router.push("/home");
  }
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [signUpButton, setSignUpButton] = useState("Sign up");
  const [loading, setLoading] = useState(false)
  const [registerInfo, setRegisterInfo] = useState("");

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setUser((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setRegisterInfo("");
    try {
      setSignUpButton("");
      setLoading(true)

      const res = await axios.post(
        "https://gatotsprinter-be.vercel.app/api/v1/user/register",
        user
      );
      console.log("Register success");

      setLoading(false)
      setSignUpButton("Done");

      router.push("/login");
    } catch (error) {
      console.log(error.response.data);
      const err = error.response.data;
      if (err.msg === "Username telah terpakai") {
        setRegisterInfo("Sign Up Failed : Username already taken");
      } else if (err.msg === "Email telah terpakai") {
        setRegisterInfo("Sign Up Failed : Email address already registered")
      } else if (err.error === "Password must be longer than 5 characters") {
        setRegisterInfo("Sign Up Failed : Password must be longer than 6 characters")
      } else if (err.error === "Username must be longer than 5 characters") {
        setRegisterInfo("Sign Up Failed : Username must be longer than 5 characters")
      }
        else if (error.response.status === 500) {
        setRegisterInfo("Sign Up Failed : Internal Server Error");
      }

      setLoading(false)
      setSignUpButton("Sign up Failed");

      setTimeout(() => {
        setSignUpButton("Sign up");
      }, 2000);
    }
  };

  return (
    <div className="main">
      <RegisterForm
        user={user}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        signUpButton={signUpButton}
        loading={loading}
        registerInfo={registerInfo}
      />
    </div>
  );
}

export default Register;
