/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    CLOUDINARY_CLOUD_NAME: process.env.CLOUDINARY_CLOUD_NAME,
    CLOUDINARY_CLOUD_PRESET: process.env.CLOUDINARY_CLOUD_PRESET,
  },
  images: {
    domains: [
      "images.unsplash.com",
      "i.ibb.co",
      "easyshiksha.com",
      "cdn.akamai.steamstatic.com",
      "play-lh.googleusercontent.com",
      "res.cloudinary.com"
    ],
  },
};

module.exports = nextConfig;
