describe('Testing User Flow in Login Page', () => {
    beforeEach(() => {
        cy.reload()
    })

    it("Login Page Should have 2 components within (Navbar and Login Form)", () => {
        cy.visit("http://localhost:3000/login");

        cy.get(".navbar").should("exist");
        cy.get("h2").invoke("text").should("match", /Sign/)
    })

    it("If user already logged in will be redirected to /home page", () => {
        cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");

        cy.visit("http://localhost:3000/login");

        cy.url().should("not.include", "/login");
        cy.url().should("include", "/home");    
    })


    it("if user typed wrong type of email it will be validated", () => {
        cy.visit("http://localhost:3000/login");
        cy.get(`input[name=email]`).type("testing123456");
        cy.get(`input[name="password"]`).type("testing123456");

        cy.get(`button[type="submit"]`).click();
        cy.get(`input[name=email]`).invoke("text").should("not.match", /@/);
    })

    it("if user have wrong password it will be showing an error", () => {
        cy.visit("http://localhost:3000/login");
        cy.get(`input[name=email]`).type("testing123456@testing.com");
        cy.get(`input[name="password"]`).type("123");

        cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/login", {
        statusCode: 400,
        body: {
            msg: "Password Salah",
        },
        }).as("loginRequest");

        cy.get(`button[type="submit"]`).click();
        cy.wait("@loginRequest");

        cy.get("h3")
        .contains("Sign in failed : Wrong password !")
        .should("exist");
    })

    it("if user email not found it will be showing an error 404", () => {
        cy.visit("http://localhost:3000/login");
        cy.get(`input[name=email]`).type("testing123456@testing.com");
        cy.get(`input[name="password"]`).type("123");

        cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/login", {
        statusCode: 404,
        body: {
            msg: "Email Tidak Ditemukan",
        },
        }).as("loginRequest");

        cy.get(`button[type="submit"]`).click();
        cy.wait("@loginRequest");

        cy.get("h3")
        .contains("Sign in failed : Wrong email or email not found !")
        .should("exist");
    })
})