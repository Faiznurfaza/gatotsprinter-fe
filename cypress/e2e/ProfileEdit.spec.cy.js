describe("Testing User Flow in Profile Edit Page", () => {
  beforeEach(() => {
    cy.reload();
  });

  it("Profile Edit page should have 2 components rendered: navbar and user edited info", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/profile/edit");
    cy.reload()
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.wait("@fetchUserInfo").then((interception) => {
      console.log(interception)
      const user = interception.response.body.data;

      cy.get(".navbar").should("exist");
      cy.get("h2").contains("EDIT USER").should("exist");
      cy.get('[for="username"]').should("contain", "Username");
      cy.get("#username").should("have.value", user.username);
      cy.get('[for="email"]').should("contain", "Email Address");
      cy.get("#email").should("have.value", user.email); // Update this line with the appropriate input selector
      cy.get('[for="oldPassword"]').should("contain", "Password");
      cy.get('[for="newPassword"]').should("contain", "New Password");
    });
  });

  it("Profile page can not be accessed by guest", () => {
    cy.clearCookie("userId");
    cy.reload();
    cy.visit("http://localhost:3000/profile/edit");

    cy.url().should("not.include", "/profile/edit");
    cy.url().should("include", "/register");
  });

  it("User can click 'Back to Profile'", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/profile/edit");
    cy.get("a[href*='profile']")
      .contains("Profile")
      .should("exist")
      .click({ force: true });
    cy.wait(6000);

    cy.url().should("include", "/profile");
  });

  it("should call the handleUpdateUsername function when submitting the username form", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile/edit");

    cy.wait("@fetchUserInfo").then((interception) => {
      const user = interception.response.body.data;
      cy.get("#username").should("have.value", user.username);
      cy.contains("Edit Username").click();
      cy.get("#username").clear().type("adittiya1");

      cy.intercept(
        {
          method: "PUT",
          url: "https://gatotsprinter-be.vercel.app/api/v1/user/edit/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        },
        {
          statusCode: 200,
          body: {
            msg: "Profile updated successfully",
          },
        }
      ).as("updateProfile");

      cy.contains("Save").click();
      cy.wait("@updateProfile", { timeout: 5000 });
      cy.get(".Toastify__toast-body").should(
        "contain",
        "Username updated successfully!"
      );
    });
  });

  it("should call the handleUpdatePassowrd function when submitting the Password form", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile/edit");

    cy.wait("@fetchUserInfo").then((interception) => {
      const user = interception.response.body.data;
      cy.contains("Edit Password").click();
      cy.get("#oldPassword").type("adittiya");
      cy.get("#newPassword").type("adittiya1");

      cy.intercept(
        {
          method: "PUT",
          url: "https://gatotsprinter-be.vercel.app/api/v1/user/edit/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        },
        {
          statusCode: 200,
          body: {
            msg: "Profile updated successfully",
          },
        }
      ).as("updateProfile");

      cy.contains("Save").click();
      cy.wait("@updateProfile", { timeout: 5000 });
      cy.get(".Toastify__toast-body").should(
        "contain",
        "Password updated successfully!"
      );
    });
  });

  it("should display an error message when the username is already in use", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile/edit");

    cy.wait("@fetchUserInfo").then((interception) => {
      const user = interception.response.body.data;
      cy.get("#username").should("have.value", user.username);
      cy.contains("Edit Username").click();
      cy.get("#username").type("1");

      cy.intercept(
        {
          method: "PUT",
          url: "https://gatotsprinter-be.vercel.app/api/v1/user/edit/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        },
        {
          statusCode: 409,
          body: {
            msg: "Username already in use",
          },
        }
      ).as("updateProfile");
      cy.contains("Save").click();
      cy.wait("@updateProfile", { timeout: 5000 });
      cy.get(".Toastify__toast-body").should(
        "contain",
        "Failed to update username."
      );
    });
  });
  it("should display an error message when the old password is invalid", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile/edit");

    cy.wait("@fetchUserInfo").then((interception) => {
      cy.contains("Edit Password").click();
      cy.get("#oldPassword").type("adittiya1");
      cy.get("#newPassword").type("adittiya1");

      cy.intercept(
        {
          method: "PUT",
          url: "https://gatotsprinter-be.vercel.app/api/v1/user/edit/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        },
        {
          statusCode: 401,
          body: {
            msg: "Invalid old password",
          },
        }
      ).as("updatePassword");

      cy.contains("Save").click();
      cy.wait("@updatePassword", { timeout: 5000 });
      cy.get(".Toastify__toast-body").should(
        "contain",
        "Failed to update password."
      );
    });
  });
  it("should display an error message when the old password or new password are missing", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile/edit");

    cy.wait("@fetchUserInfo").then((interception) => {
      cy.contains("Edit Password").click();
      cy.get("#oldPassword").type("adittiya1");
      cy.get("#newPassword").type("adittiya1");

      cy.intercept(
        {
          method: "PUT",
          url: "https://gatotsprinter-be.vercel.app/api/v1/user/edit/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
        },
        {
          statusCode: 400,
          body: {
            msg: "Old password and new password are required",
          },
        }
      ).as("updatePassword");

      cy.contains("Save").click();
      cy.wait("@updatePassword", { timeout: 5000 });
      cy.get(".Toastify__toast-body").should(
        "contain",
        "Failed to update password."
      );
    });
  });
});
