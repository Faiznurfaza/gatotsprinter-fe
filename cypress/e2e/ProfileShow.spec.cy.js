describe("Testing User Flow in ProfileShow Page", () => {
  beforeEach(() => {
    cy.reload();
  });
  it("ProfileShow page should have 2 components rendered: navbar and user info", () => {
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    ).as("fetchUserInfo");

    cy.visit(
      "http://localhost:3000/profile/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    );

    cy.wait("@fetchUserInfo").then((interception) => {
      const user = interception.response.body.data;
      const username = user.username.toUpperCase();
      const formattedDate = new Date(user.createdAt).toLocaleDateString(
        "en-US",
        {
          day: "numeric",
          month: "long",
          year: "numeric",
        }
      );
      cy.get(".navbar").should("exist");
      cy.get("h2").contains(username).should("exist");
      cy.get("h3")
        .invoke("text")
        .should("match", /Player Info/);
      cy.get("td")
        .contains("Registered:")
        .next()
        .should("contain", formattedDate);
    });
  });

  it("User can click 'Back Home'", () => {
    cy.setCookie("userId", "72c68f91-0a7e-48f5-b285-bef4299ebea8");
    cy.visit(
      "http://localhost:3000/profile/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    );
    cy.get("a[href*=home]")
      .contains("Back Home")
      .should("exist")
      .click({ force: true });

    cy.wait(6000);

    cy.url().should("include", "/home");
  });
  it("Navbar shouldn't render profile and logout button for guest", () => {
    cy.visit(
      "http://localhost:3000/profile/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    );
    cy.get(".navbar").should("exist");
    cy.get('a[href*="/profile"]').should("not.exist");
    cy.get('a[href*="/logout"]').should("not.exist");
  });

  it("Navbar shouldn't render Sign up and Sign in button for user logged in ", () => {
    cy.setCookie("userId", "72c68f91-0a7e-48f5-b285-bef4299ebea8");
    cy.visit(
      "http://localhost:3000/profile/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    );
    cy.reload();

    cy.get(".navbar").should("exist");
    cy.get("a[href*=register]").should("not.exist");
    cy.get("a[href*=login]").should("not.exist");
  });

  it("User Info component shouldn't render for invalid user ID in URL params", () => {
    const invalidUserId = "invalid-user-id";

    cy.visit(`http://localhost:3000/profile/${invalidUserId}`);
    cy.get(".navbar").should("exist");
    cy.get("h2").should("not.exist");
    cy.get("td").should("not.exist");
  });
});
