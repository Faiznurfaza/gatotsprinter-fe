describe("Testing User Flow in Register Page", () => {
  beforeEach(() => {
    cy.reload();
  });

  it("Register Page should have 2 Components within (Navbar and Register Form)", () => {
    cy.visit("http://localhost:3000/register");

    cy.get(".navbar").should("exist");
    cy.get("h2").invoke("text").should("match", /Sign/);
  });

  it("If user succesfully register it will redirect user to /login page", () => {
    cy.visit("http://localhost:3000/register");

    cy.get("input[name=username]").type("testing123456");
    cy.get(`input[name=email]`).type("testing123456@testing.com");
    cy.get(`input[name="password"]`).type("testing123456");

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/register", {
      statusCode: 201,
      body: {
        msg: "Registrasi Berhasil",
      },
    }).as("registerRequest");

    cy.get(`button[type="submit"]`).click();
    cy.wait("@registerRequest");
    cy.contains("Done").should("be.visible");
    cy.url().should("include", "/login");
  });

  it("If user dont fill username field correctly and then clicks submit will showing an error", () => {
    cy.visit("http://localhost:3000/register");
    cy.get("input[name=username]").type("1234");
    cy.get(`input[name=email]`).type("testing123456@testing.com");
    cy.get(`input[name="password"]`).type("testing123456");

    cy.get(`button[type="submit"]`).click();

    cy.get("h3")
      .contains("Sign Up Failed : Username must be longer than 5 characters")
      .should("exist");
  });

  it("if user dont fill email address field correctly and then clicks submit it will be validated by browser", () => {
    cy.visit("http://localhost:3000/register");
    cy.get("input[name=username]").type("testing123456");
    cy.get(`input[name=email]`).type("testing123456");
    cy.get(`input[name="password"]`).type("testing123456");

    
    cy.get(`button[type="submit"]`).click();
    cy.get(`input[name=email]`).invoke("text").should("not.match", /@/);
  });

  it("if user dont fill password field correctly and then clicks submit it will showing an error", () => {
    cy.visit("http://localhost:3000/register");
    cy.get("input[name=username]").type("testing123456");
    cy.get(`input[name=email]`).type("testing123456@testing.com");
    cy.get(`input[name="password"]`).type("123");

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/register", {
      statusCode: 400,
      body: {
        error: "Password must be longer than 5 characters",
      },
    }).as("registerRequest");

    cy.get(`button[type="submit"]`).click();
    cy.wait("@registerRequest");

    cy.get("h3")
      .contains("Sign Up Failed : Password must be longer than 6 characters")
      .should("exist");
  })

  it("If user register with username that already taken it will showing an error", () => {
    cy.visit("http://localhost:3000/register");
    cy.get("input[name=username]").type("Noobmaster69");
    cy.get(`input[name=email]`).type("testing123456@testing.com");
    cy.get(`input[name="password"]`).type("testing123456");

    cy.get(`button[type="submit"]`).click();

    cy.get("h3")
      .contains("Sign Up Failed : Username already taken")
      .should("exist");
  })

  it("If user register with email that already taken it will showing an error", () => {
    cy.visit("http://localhost:3000/register");
    cy.get("input[name=username]").type("testing123456");
    cy.get(`input[name=email]`).type("binarchallenge9@gmail.com");
    cy.get(`input[name="password"]`).type("testing123456");

    cy.get(`button[type="submit"]`).click();

    cy.get("h3")
      .contains("Sign Up Failed : Email address already registered")
      .should("exist");
  })

  it("if user correctly fill all the register form but theres unexpected error happens on the back-end, it will showing an error to the user", () => {
    cy.visit("http://localhost:3000/register");

    cy.get("input[name=username]").type("testing123456");
    cy.get(`input[name=email]`).type("testing123456@testing.com");
    cy.get(`input[name="password"]`).type("testing123456");

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/register", {
      statusCode: 500,
      body: {
        error: 'Internal server error',
      },
    }).as("registerRequest");

    cy.get(`button[type="submit"]`).click();
    cy.wait("@registerRequest");
    cy.get("h3")
      .contains("Sign Up Failed : Internal Server Error")
      .should("exist");
  })
});
