describe("Testing User Flow in Game List Page", () => {
  beforeEach(() => {
    cy.reload
  });

  it("Game List Page can be accesses", () => {
    cy.visit('http://localhost:3000/games')
    cy.url().should('include', '/games')
  })

  it("Game List Page should have 3 Components within (Navbar, Header and Game List)", () => {
    cy.visit("http://localhost:3000/games");
    cy.get(".navbar").should("exist");
    cy.get("h5").contains("Game List").should("exist");
    cy.get("h1").contains("Choose the game that you want to play").should("exist");
    cy.get(".game-card").should("exist");
  });

  it("navbar shouldn't render profile and logout button for guest", () => {
    cy.visit('http://localhost:3000/games')
    cy.get(".navbar").should("exist");
    cy.get('a[href*="/profile"]').should("not.exist");
    cy.get('a[href*="/logout"]').should("not.exist");
  });  
    
  it("navbar shouldn't render Sign up and Sign in button for user logged in ", () => {
    cy.setCookie("userId", "57e8ce20-814d-48dc-aeb7-4b8c4294a072");
    cy.visit('http://localhost:3000/games')
    cy.reload();

    cy.get(".navbar").should("exist");
    cy.get("a[href*=register]").should("not.exist");
    cy.get("a[href*=login]").should("not.exist");
  });

  it("Game List Components should be rendered properly", () => {
    cy.visit("http://localhost:3000/games");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/game/all"
    ).as("fetchGames");

    cy.wait("@fetchGames").then((interception) => {
      const game = interception.response.body.games;
      cy.get('div.game-card').each(($card) => {
        game.map(game => {
          if ($card.attr('key') === `${game.id}`) {
            cy.get('h4').should('contain', `${game.title}`);
            cy.get(`/games/${game.id}`).should("exist");
            cy.get(`/games/${game.id}/play`).should("exist");
          }
        })
      })
    });
  })

  it('if something wrong (Internal Server Error) will show error 500', () => {
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/game/all",
      {
        statusCode: 500,
        body: {
            error: "Internal server error"
        },
      }
    ).as("fetchAllGames");
    cy.visit('http://localhost:3000/games')
    cy.reload
    cy.wait('@fetchAllGames')
  })

});
