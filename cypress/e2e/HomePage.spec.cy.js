describe("Testing User Flow in Home Page", () => {
  beforeEach(() => {
    cy.reload();
  });

  it("Home Page can be accessed by authenticated user", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/home");
    cy.url().should("include", "/home");
  });

  it("Home page should have 2 components rendered: navbar and user info", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/home");
    cy.wait(3000)
    cy.get(".navbar").should("exist");
    cy.get("h1")
      .invoke("text")
      .should("match", /Welcome/);
  });

  it("Home page can not be accessed by guest", () => {
    cy.clearCookie("userId");
    cy.reload();
    cy.visit("http://localhost:3000/home");
    cy.wait(3000)
    cy.url().should("not.include", "/home");
    cy.url().should("include", "/login");
  });

  it("User Info Components should be rendered properly", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/home");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9"
    ).as("fetchUserInfo");
    cy.wait(3000)
    cy.wait("@fetchUserInfo").then((interception) => {
      console.log(interception.response)
      const user = interception.response.body.data;
      cy.get("h1").contains(`Welcome`).should("exist");
      cy.get("div.profile-photo").should("exist");
      cy.get("p").contains(`Email `).should("exist");
    });
  });

  it("User can click 'view my profile'", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/home");
    cy.reload()
    cy.wait(3000)
    cy.contains("View My Profile", { timeout: 10000 }).should("exist").click({ force: true});
    cy.url().should("include", "/profile");
  });

  it("If userId cookies is presence but the userId isnt found in the database, User Info component shouldn't be rendered", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d1");
    cy.visit("http://localhost:3000/home");
    cy.reload();
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d1",
      {
        statusCode: 400,
        body: {
          result: "ERROR",
          msg: "Error occured while retrieving data",
        },
      }
    ).as("fetchUserInfo");
    cy.wait(5000)
    cy.wait("@fetchUserInfo");
    cy.get("div.profile-photo").should("not.exist");
  });

  it("if userId cookies is presence but the userId isnt found in the database, it should redirecting user to login page and destroy the session", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d1");
    cy.visit("http://localhost:3000/home");
    cy.reload();
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d1",
      {
        statusCode: 400,
        body: {
          result: "ERROR",
          msg: "Error occured while retrieving data",
        },
      }
    ).as("fetchUserInfo");
    cy.wait(3000)
    cy.wait("@fetchUserInfo");

    cy.url().should("eq", "http://localhost:3000/login");
    cy.getCookie("userId").should("not.exist");
  });

  it("User info components shouldnt be rendered if request API failed (error 500) and redirecting user to login page", () => {
    cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
    cy.visit("http://localhost:3000/home");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/7475cb77-71e0-49ba-902f-ddb079f75750",
      {
        statusCode: 500,
        body: "Internal server error",
      }
    ).as("fetchUserInfo");
    cy.get("div.profile-photo").should("not.exist");
    cy.get("p").should("not.exist");
    cy.wait("@fetchUserInfo");
    cy.wait(3000)
    cy.url().should("include", "/login")
    cy.getCookie("userId").should("not.exist")
  });
});
