describe("Testing User Flow in Logout Page", () => {
    beforeEach(() => {
        cy.reload
    });

    it("navbar and logout popup should exist", () => {
        cy.visit('http://localhost:3000/logout')
        cy.get(".navbar").should("exist");
        cy.get("h2").contains("Are you sure want to logout ?");
        cy.get("button.logout-button").should("exist");
        cy.get("button.cancel-button").should("exist");
    });

    it("navbar shouldn't render profile and logout button for guest", () => {
      cy.visit('http://localhost:3000/logout')
      cy.get(".navbar").should("exist");
      cy.get('a[href*="/profile"]').should("not.exist");
      cy.get('a[href*="/logout"]').should("not.exist");
    });  
    
    it("navbar shouldn't render Sign up and Sign in button for user logged in ", () => {
        cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
        cy.visit('http://localhost:3000/logout')
        cy.reload();

        cy.get(".navbar").should("exist");
        cy.get("a[href*=register]").should("not.exist");
        cy.get("a[href*=login]").should("not.exist");
    });

    it("Logout Button", () => {
        cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
        cy.visit('http://localhost:3000/logout')
        cy.get("button.logout-button").should("exist").click();
        cy.url().should('include', '/login');
    });
    
    it("Cancel Button", () => {
        cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
        cy.visit('http://localhost:3000/logout')
        cy.get("button.cancel-button").should("exist").click();
        cy.url().should('include', '/home');
    });
    
    it("Logout Button return (Internal Server Error) will show error 500", () => {
        cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
        cy.visit('http://localhost:3000/logout');
        cy.intercept(
            "POST",
            "https://gatotsprinter-be.vercel.app/api/user/logout",
            {
                statusCode: 500,
                body: {
                    error: "Internal server error"
                },
            }
        ).as("userLogout");
        cy.get("button.logout-button").should("exist").click();
        cy.reload
        cy.wait('@userLogout')
    });
});