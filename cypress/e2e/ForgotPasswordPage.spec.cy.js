describe('Testing user flow in forgot password page', () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/forgotpassword")
  })

  it('If user visit forgot password page, it should render 2 components (Navbar and Forgot password form)', () => {
    cy.get(".navbar").should("exist")
    cy.get("input[type=email]").should("exist")
  })

  it('If user fill the form of forgot password with existing and valid email and click submit, it will showing an reset password code sent to email', () => {
    cy.get(".navbar").should("exist")
    cy.get("input[type=email]").should("exist").type("binarchallenge9@gmail.com")

    cy.get(`button[type="submit"]`).click();
    cy.wait(6000)
    cy.contains("Done").should("be.visible");
    cy.contains("Reset Password Code is sent to your registered email").should("be.visible")
  })

  it("If user fill the form of forgot password with unregistered email and click submit", () => {
    cy.get(".navbar").should("exist")
    cy.get("input[type=email]").should("exist").type("binarchallenge9@gmail.com")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/forgot-password", {
      statusCode: 404,
      body: {
        msg: "Email tidak ditemukan",
      },
    }).as("forgotpassword");
    cy.get(`button[type="submit"]`).click();
    cy.wait("@forgotpassword")
    cy.contains("Done").should("not.exist");
    cy.contains("Email address not registered, try again with registered email address").should("be.visible")
  })

  it("If user fill the form of forgot password with invalid email, browser should validate the email address", () => {
    cy.get(".navbar").should("exist")
    cy.get("input[type=email]").should("exist").type("binarchallenge9")

    cy.get(`button[type="submit"]`).click();
    cy.get(`input[name=email]`).invoke("text").should("not.match", /@/);
  })

  it("If user fill the form of forgot password with registered email and clicks submit, but theres an unexpected error happens", () => {
    cy.get(".navbar").should("exist")
    cy.get("input[type=email]").should("exist").type("binarchallenge9@gmail.com")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/forgot-password", {
      statusCode: 500,
      body: {
        msg: "Internal server error",
      },
    }).as("forgotpassword");
    cy.get(`button[type="submit"]`).click();
    cy.wait("@forgotpassword")
    cy.wait(6000)
    cy.contains("Done").should("not.exist");
    cy.contains("Unexpected error happens, please try again later.").should("be.visible")
  })
})