describe('Testing for LeaderBoard Page Flow', () => {
    beforeEach(() => {
        cy.reload
    })

    it("LeaderBoard Page can be accesses", () => {
        cy.visit('http://localhost:3000/leaderboard')
        cy.url().should('include', '/leaderboard')
    })

    it('Leaderboard Page should have 2 components rendered (Navbar and Leaderboard)', () => {
        cy.visit('http://localhost:3000/leaderboard')
        cy.get(".navbar").should("exist");  
        cy.get("h1")
        .invoke("text")
        .should("match", /Let's Compete/);  
    })
    it('if something wrong (Forbidden Action) will show error 403', () => {
        cy.intercept(
            "GET",
            "https://gatotsprinter-be.vercel.app/api/v1/score/all",
            {
              statusCode: 403,
              body: {
                  Error: "Forbidden action"
              },
            }
          ).as("fetchScore");
        cy.visit('http://localhost:3000/leaderboard')
        cy.reload
        cy.wait('@fetchScore')
    })
    it('if something wrong (Internal Server Error) will show error 500', () => {
        cy.intercept(
            "GET",
            "https://gatotsprinter-be.vercel.app/api/v1/score/all",
            {
              statusCode: 500,
              body: {
                  error: "Internal server error"
              },
            }
          ).as("fetchAllScore");
        cy.visit('http://localhost:3000/leaderboard')
        cy.reload
        cy.wait('@fetchAllScore')
    })
    it("navbar shouldn't render profile and logout button for guest", () => {
        cy.visit('http://localhost:3000/leaderboard')
        cy.get(".navbar").should("exist");
        cy.get('a[href*="/profile"]').should("not.exist");
        cy.get('a[href*="/logout"]').should("not.exist");
      });  
      
    it("navbar shouldn't render Sign up and Sign in button for user logged in ", () => {
        cy.setCookie("userId", "57e8ce20-814d-48dc-aeb7-4b8c4294a072");
        cy.visit('http://localhost:3000/leaderboard')
        cy.reload();
    
        cy.get(".navbar").should("exist");
        cy.get("a[href*=register]").should("not.exist");
        cy.get("a[href*=login]").should("not.exist");
    });
    
})