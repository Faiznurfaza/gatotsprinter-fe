describe('Testing user flow in reset password page', () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/resetpassword")
  })

  it('Should render 2 components properly', () => {
    cy.get(".navbar").should("exist")
    cy.get("input[name=resetToken]").should("exist")
    cy.get("input[name=password]").should("exist")
  })

  it("If user fill the form correctly and click submit, it will showing success message", () => {
    cy.get("input[name=resetToken]").type("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzZTM0YmZkMy04NWVlLTRmNTgtYjVlMi1kMjg5Yjc2MjJhMTEiLCJlbWFpbCI6ImJpbmFyY2hhbGxlbmdlOUBnbWFpbC5jb20iLCJpYXQiOjE2ODc5NjM3MTcsImV4cCI6MTY4Nzk2NzMxN30.S9S6ROvkG-sowmHWcSQJb7WRN4_2w5JXS_-73M9KPGA")
    cy.get("input[name=password]").type("newpassword123")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password", {
      statusCode: 200,
      body: {
        msg: 'Reset password berhasil'
      },
    }).as("resetpassword")
    cy.get("button[type=submit]").click()

    cy.wait("@resetpassword")

    cy.contains("Done").should("exist")
    cy.contains("Reset Password Success, you can go try login again with your new password.")
  })

  it("if user fill the form correctly and click submit, and user decided to clicks LOGIN HERE", () => {
    cy.get("input[name=resetToken]").type("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzZTM0YmZkMy04NWVlLTRmNTgtYjVlMi1kMjg5Yjc2MjJhMTEiLCJlbWFpbCI6ImJpbmFyY2hhbGxlbmdlOUBnbWFpbC5jb20iLCJpYXQiOjE2ODc5NjM3MTcsImV4cCI6MTY4Nzk2NzMxN30.S9S6ROvkG-sowmHWcSQJb7WRN4_2w5JXS_-73M9KPGA")
    cy.get("input[name=password]").type("newpassword123")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password", {
      statusCode: 200,
      body: {
        msg: 'Reset password berhasil'
      },
    }).as("resetpassword")
    cy.get("button[type=submit]").click()

    cy.wait("@resetpassword")

    cy.contains("Done").should("exist")
    cy.get("a[href*=login]").contains("LOGIN HERE").click()
    cy.url().should("include", "/login")
  })

  it("If user fill the form but the new password is less than 5 characters, it will showing an error", () => {
    cy.get("input[name=resetToken]").type("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzZTM0YmZkMy04NWVlLTRmNTgtYjVlMi1kMjg5Yjc2MjJhMTEiLCJlbWFpbCI6ImJpbmFyY2hhbGxlbmdlOUBnbWFpbC5jb20iLCJpYXQiOjE2ODc5NjM3MTcsImV4cCI6MTY4Nzk2NzMxN30.S9S6ROvkG-sowmHWcSQJb7WRN4_2w5JXS_-73M9KPGA")
    cy.get("input[name=password]").type("12345")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password", {
      statusCode: 400,
      body: {
        error: "Password must be longer than 5 characters"
      },
    }).as("resetpassword")
    cy.get("button[type=submit]").click()

    cy.contains("Done").should("not.exist")
    cy.get("a[href*=login]").contains("LOGIN HERE").should("not.exist")
    cy.contains("Reset Password Failed : Password must be longer than 5 characters")
  })

  it("If user fill the form but the Reset Token is invalid, it will showing an error", () => {
    cy.get("input[name=resetToken]").type("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzZTM0YmZkMy04NWVlLTRmNTgtYjVlMi1kMjg5Yjc2MjJhMTEiLCJlbWFpbCI6ImJpbmFyY2hhbGxlbmdlOUBnbWFpbC5jb20iLCJpYXQiOjE2ODc5NjM3MTcsImV4cCI6MTY4Nzk2NzMxN30.S9S6ROvkG-sowmHWcSQJb7WRN4_2w5JXS_-73M9KPGA")
    cy.get("input[name=password]").type("123456789")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password", {
      statusCode: 400,
      body: {
        msg: 'Token reset password tidak valid'
      },
    }).as("resetpassword")
    cy.get("button[type=submit]").click()

    cy.contains("Done").should("not.exist")
    cy.get("a[href*=login]").contains("LOGIN HERE").should("not.exist")
    cy.contains("Reset Password Failed, your reset token is invalid")
  })

  it("If user fill the form correctly, but unexpected error happens on the back-end it will showing an error", () => {
    cy.get("input[name=resetToken]").type("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzZTM0YmZkMy04NWVlLTRmNTgtYjVlMi1kMjg5Yjc2MjJhMTEiLCJlbWFpbCI6ImJpbmFyY2hhbGxlbmdlOUBnbWFpbC5jb20iLCJpYXQiOjE2ODc5NjM3MTcsImV4cCI6MTY4Nzk2NzMxN30.S9S6ROvkG-sowmHWcSQJb7WRN4_2w5JXS_-73M9KPGA")
    cy.get("input[name=password]").type("123456789")

    cy.intercept("POST", "https://gatotsprinter-be.vercel.app/api/v1/user/reset-password", {
      statusCode: 500,
      body: {
        error: 'Internal server error'
      },
    }).as("resetpassword")
    cy.get("button[type=submit]").click()

    cy.contains("Done").should("not.exist")
    cy.get("a[href*=login]").contains("LOGIN HERE").should("not.exist")
    cy.contains("Reset Password Failed, try again later")
  })
})