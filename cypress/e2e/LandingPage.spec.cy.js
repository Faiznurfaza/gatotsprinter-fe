describe("Testing User Flow in Landing Page", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });
  it("should render navbar, headersection, leaderboardsection, newsletter, footersection components", () => {
    cy.get(".navbar").should("exist");
    cy.get("h2").contains("Play with us").should("exist");
    cy.get("h2").contains("Meet Our Top Scores").should("exist");
    cy.get("h2").contains("Subscribe to our newsletter.").should("exist");
    cy.get(".footer-text").contains("Copyright © 2023 Gatot Sprinter All Rights reserved.").should("exist")
  });

  it("should render navbar for authenticated user", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.reload();

    cy.get(".navbar").should("exist");

    cy.get(".navbar").within(() => {
      cy.get(".nav-logo").should("exist");
      cy.get('a[href*="/home"]').should("exist");
      cy.get('a[href*="/games"]').should("exist");
      cy.get('a[href*="/leaderboard"]').should("exist");
      cy.get('a[href*="/profile"]').should("exist");
      cy.get('a[href*="/logout"]').should("exist");
    });
  });

  it("should render navbar for unathenticated user (guest)", () => {
    cy.get(".navbar").should("exist");
    cy.get(".navbar").within(() => {
      cy.get(".nav-logo").should("exist");
      cy.get('a[href*="/home"]').should("exist");
      cy.get('a[href*="/games"]').should("exist");
      cy.get('a[href*="/leaderboard"]').should("exist");
      cy.get('a[href*="/login"]').should("exist");
      cy.get('a[href*="/register"]').should("exist");
    });
  });

  it("navbar shouldn't render profile and logout button for unauthenticated user (guest)", () => {
    cy.get(".navbar").should("exist");
    cy.get('a[href*="/profile"]').should("not.exist");
    cy.get('a[href*="/logout"]').should("not.exist");
  });

  it("navbar shouldn't render register login button for authenticated user", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.reload();

    cy.get(".navbar").should("exist");
    cy.get("a[href*=register]").should("not.exist");
    cy.get("a[href*=login]").should("not.exist");
  });

  it("should not allow guest to access /home from navbar", () => {
    cy.get(".navbar").should("exist");
    cy.get("a[href*=home]").should("exist").click();
    cy.wait(3000)
    cy.url().should("not.include", "/home");
    cy.url().should("include", "/login");
  });

  it("should allow user to access /home from navbar", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.reload();
    cy.get(".navbar").should("exist");
    cy.get("a[href*=home]").should("exist").click();
    cy.wait(3000)
    cy.url().should("not.include", "/login");
    cy.url().should("include", "/home");
  });

  it("should allow anyone to access /games from navbar", () => {
    cy.get(".navbar").should("exist");
    cy.get(".navbar").within(() => {
      cy.get("a[href*=games]").should("exist").click();
    });
    cy.url().should("include", "/games");
  });

  it("should allow anyone to access /leaderboard from navbar", () => {
    cy.get(".navbar").should("exist");
    cy.get(".navbar").within(() => {
      cy.get("a[href*=leaderboard]").should("exist").click();
    });
    cy.url().should("include", "/leaderboard");
  });

  it("leaderboard component should fetched data from API", () => {
    cy.intercept("GET", "https://gatotsprinter-be.vercel.app/api/v1/score/all").as(
      "fetchLeaderboard"
    );

    cy.wait("@fetchLeaderboard");

    cy.get(".leaderboard").should("exist");
    cy.get("h3.username-text").should("exist");
  });

  it("Leaderboard component shouldn't render empty data from API", () => {
    cy.intercept("GET", "https://gatotsprinter-be.vercel.app/api/v1/score/all", {
      fixture: "emptyLeaderboard.json",
    }).as("emptyLeaderboard");

    cy.wait("@emptyLeaderboard");

    cy.get(".leaderboard").should("not.exist");
    cy.get("h3.username-text").should("not.exist");
  });
});
