describe('Testing for user flow in video page', () => {
  it('should render 2 components if authenticated user visit, navbar and video info', () => {
    cy.setCookie('userId', '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9')
    cy.visit('http://localhost:3000/video')
    cy.reload()

    cy.get(".navbar").should("exist")
    cy.get("h2").contains("Video Streaming").should("exist")
    cy.get('h2').contains("Upload Video").should("exist")
    cy.get('h2').contains("User Video").should("exist")
  })

  it('shouldnt allow guest to visit this page', () => {
    cy.visit('http://localhost:3000/video')
    cy.reload()

    cy.get(".navbar").should("exist")
    cy.get("h2").contains("Video Streaming").should("not.exist")
    cy.get('h2').contains("Upload Video").should("not.exist")
    cy.get('h2').contains("User Video").should("not.exist")
  })

  it('if user click stream here, it will redirecting user to /video/stream', () => {
    cy.setCookie('userId', '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9')
    cy.visit('http://localhost:3000/video')
    cy.reload()

    cy.contains("Stream Here").should("exist").click()

    cy.url('/video/stream').should("exist")
  })

  it('if user click upload here, it will redirecting user to /video/upload', () => {
    cy.setCookie('userId', '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9')
    cy.visit('http://localhost:3000/video')
    cy.reload()

    cy.contains("Upload Here").should("exist").click()

    cy.url('/video/upload').should("exist")
  })

  it('if user click manage here, it will redirecting user to /video/list', () => {
    cy.setCookie('userId', '1c7672a3-bdb2-44ac-acaa-9da2fd4436d9')
    cy.visit('http://localhost:3000/video')
    cy.reload()

    cy.contains("Manage Here").should("exist").click({force: true})

    cy.url('/video/list').should("exist")
  })
})