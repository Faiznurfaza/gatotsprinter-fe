describe("Testing User Flow in Game Detail Page", () => {
  beforeEach(() => {
    cy.reload
  });

  it("navbar shouldn't render profile and logout button for guest", () => {
    cy.visit('http://localhost:3000/games/1')
    cy.get(".navbar").should("exist");
    cy.get('a[href*="/profile"]').should("not.exist");
    cy.get('a[href*="/logout"]').should("not.exist");
  });  
    
  it("navbar shouldn't render Sign up and Sign in button for user logged in ", () => {
    cy.setCookie("userId", "57e8ce20-814d-48dc-aeb7-4b8c4294a072");
    cy.visit('http://localhost:3000/games/1')
    cy.reload();

    cy.get(".navbar").should("exist");
    cy.get("a[href*=register]").should("not.exist");
    cy.get("a[href*=login]").should("not.exist");
  });

  it("Game Detail Page Header, Game Info Contents, Leaderboard Contents, Preview Contents", () => {
    cy.visit("http://localhost:3000/games/1");

    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/game/id/1"
    ).as("getGame");

    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/score/all"
    ).as("fetchScore");

    cy.get(".navbar").should("exist");
    cy.get("div.section-1").contains("Game Info").should("exist");
    cy.get("div.section-2").contains("Top 5 Leaderboard").should("exist");
    cy.get("div.section-3").contains("Preview").should("exist");
    
    cy.wait("@getGame").then((interception) => {
      const game = interception.response.body.game;
      cy.get("div.title-header").contains(`${game.title}`).should("exist");
      cy.get("h1.quest-1").contains(`What is ${game.title} ?`).should("exist");
      cy.get("h1.quest-2").contains("How to play ?").should("exist");
      cy.get("h1.quest-3").contains("Release Date").should("exist");
      cy.get("h1.quest-4").contains("Latest Update").should("exist");
      cy.get("p.ans-1").contains(`${game.description}`).should("exist");
      cy.get("p.ans-2").contains(`${game.how_to_play}`).should("exist");
      cy.get("p.ans-3").contains(`${new Date(game.release_date).toLocaleDateString()}`).should("exist");
      cy.get("p.ans-4").contains(`${game.latest_update}`).should("exist");
    });

    cy.wait("@fetchScore").then((interception) => {
      const scores = interception.response.body.scores;
      cy.get("div.leaderboard-list").should("exist");
    });

    cy.get(".vid-player").should("exist");
  });

  it("Click Button See More and visit leaderboard page", () => {
    cy.visit("http://localhost:3000/games/1");

    cy.get("a[href*=leaderboard]")
      .contains("See More....")
      .should("exist")
      .click({ force: true });

    cy.url().should("include", "/leaderboard");
  });

  it('if something wrong (Internal Server Error) will show error 500', () => {
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/game/id/1",
      {
        statusCode: 500,
        body: {
            error: "Internal server error"
        },
      }
    ).as("fetchGame");
    cy.visit('http://localhost:3000/games/1')
    cy.reload
    cy.wait('@fetchGame')
  })

  it('if game not found (Error occured while retrieving data) will show error 400', () => {
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/game/id/1",
      {
        statusCode: 400,
        body: {
          result: 'ERROR',
          msg: 'Error occured while retrieving data'
        },
      }
    ).as("fetchGameById");
    cy.visit('http://localhost:3000/games/1')
    cy.reload
    cy.wait('@fetchGameById')
  })

});