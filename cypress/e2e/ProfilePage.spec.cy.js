describe("Testing User Flow in Profile Page", () => {
  beforeEach(() => {
    cy.reload();
  });
  it("Profile page should have 2 components rendered: navbar and user info", () => {
    cy.setCookie("userId", "72c68f91-0a7e-48f5-b285-bef4299ebea8");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/72c68f91-0a7e-48f5-b285-bef4299ebea8"
    ).as("fetchUserInfo");

    cy.visit("http://localhost:3000/profile");

    cy.wait("@fetchUserInfo").then((interception) => {
      const user = interception.response.body.data;
      const username = user.username.toUpperCase();

      cy.get(".navbar").should("exist");
      cy.get("h2").contains(username).should("exist");
      cy.get("h3")
        .invoke("text")
        .should("match", /Player Info/);
      cy.get("td").contains("Email:").next().should("contain", user.email);
    });
  });

  it("Profile page can not be accessed by guest", () => {
    cy.clearCookie("userId");
    cy.reload();
    cy.visit("http://localhost:3000/profile");

    cy.url().should("not.include", "/profile");
    cy.url().should("include", "/register");
  });

  it("User can click 'Edit Profile'", () => {
    cy.setCookie("userId", "72c68f91-0a7e-48f5-b285-bef4299ebea8");
    cy.visit("http://localhost:3000/profile/");
    cy.get("a[href*='profile/edit']")
      .contains("Edit Profile")
      .should("exist")
      .click({ force: true });
    cy.wait(6000);

    cy.url().should("include", "/profile/edit");
  });

  it("navbar should render Sign up and Sign in button for user logged in ", () => {
    cy.setCookie("userId", "72c68f91-0a7e-48f5-b285-bef4299ebea8");
    cy.visit("http://localhost:3000/profile/");
    cy.reload();

    cy.get(".navbar").should("exist");
    cy.get('a[href*="/profile"]').should("exist");
    cy.get('a[href*="/logout"]').should("exist");
  });
  it("If userId cookie is present but the userId isn't found in the database, User Info component shouldn't be rendered", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/profile");
    cy.reload();
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
      {
        statusCode: 400,
        body: {
          result: "ERROR",
          msg: "Error occurred while retrieving data",
        },
      }
    ).as("fetchUserInfo");

    cy.wait("@fetchUserInfo");
    cy.get("h2").should("not.exist");
    cy.get("td").should("not.exist");
  });

  it("User Info components shouldn't be rendered if the API request fails (error 400)", () => {
    cy.setCookie("userId", "1c7672a3-bdb2-44ac-acaa-9da2fd4436d9");
    cy.visit("http://localhost:3000/profile");
    cy.reload();
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/1c7672a3-bdb2-44ac-acaa-9da2fd4436d9",
      {
        statusCode: 400,
        body: {
          result: "ERROR",
          msg: "Error occurred while retrieving data",
        },
      }
    ).as("fetchUserInfo");
    cy.get("h2").should("not.exist");
    cy.get("td").should("not.exist");
    cy.wait("@fetchUserInfo");
  });

  it("User Info components shouldn't be rendered if the API request fails (error 500)", () => {
    cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
    cy.visit("http://localhost:3000/profile");
    cy.intercept(
      "GET",
      "https://gatotsprinter-be.vercel.app/api/v1/user/7475cb77-71e0-49ba-902f-ddb079f75750",
      {
        statusCode: 500,
        body: "Internal server error",
      }
    ).as("fetchUserInfo");
    cy.get("h2").should("not.exist");
    cy.get("td").should("not.exist");
    cy.wait("@fetchUserInfo");
  });
});
